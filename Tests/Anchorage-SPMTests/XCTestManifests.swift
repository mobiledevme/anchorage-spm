import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(Anchorage_SPMTests.allTests),
    ]
}
#endif
