import XCTest

import Anchorage_SPMTests

var tests = [XCTestCaseEntry]()
tests += Anchorage_SPMTests.allTests()
XCTMain(tests)
